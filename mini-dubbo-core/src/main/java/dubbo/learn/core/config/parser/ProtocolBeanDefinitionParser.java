package dubbo.learn.core.config.parser;

import dubbo.learn.core.component.export.ExportService;
import dubbo.learn.core.component.protocol.ServiceProtocol;
import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.xml.AbstractSingleBeanDefinitionParser;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Element;

/**
 * <dubbo:protocol></dubbo:protocol>的解析
 */
public class ProtocolBeanDefinitionParser extends AbstractSingleBeanDefinitionParser {
    private static final String NAME = "name";

    private static final String PROTOCOL_NAME = "protocolName";

    private static final String PORT = "port";

    private static final String HOST = "host";


    @Override
    protected boolean shouldGenerateIdAsFallback() {
        return true;
    }

    @Override
    protected Class<?> getBeanClass(Element element) {
        return ServiceProtocol.class;
    }

    @Override
    protected void doParse(Element element, ParserContext parserContext, BeanDefinitionBuilder builder) {
        builder.addPropertyValue(PROTOCOL_NAME, element.getAttribute(NAME));
        builder.addPropertyValue(HOST, element.getAttribute(HOST));
        builder.addPropertyValue(PORT, element.getAttribute(PORT));

    }

}
