package dubbo.learn.common.contract.registry;

import dubbo.learn.common.RpcContext;

/**
 * 注册中心层的rpc调用者
 * 1：接收上层传下来的业务参数，并返回结果
 *
 * 本层：会根据不同实现，去相应的注册中心，获取匹配的服务提供者列表，传输给下一层
 */
public interface RegistryLayerRpcInvoker {

    Object invoke(RpcContext rpcContext);
}
