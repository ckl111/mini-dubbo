package dubbo.learn.common.contract.exchange;

import dubbo.learn.common.ProviderHostAndPort;
import dubbo.learn.common.RpcContext;

import java.util.List;

/**
 * 本层的上层为cluster层，下层为transport层。
 * 上层cluster根据集群容错、负载均衡策略，确定了要调用的服务提供者之后，交给本层处理。
 * 本层会完成类似同步转异步的操作
 */
public interface ExchangeLayerRpcInvoker {

    /**
     *
     * @param providerHostAndPort 要调用的服务提供者的地址
     * @param rpcContext   rpc上下文，包含了要调用的参数等
     * @return  rpc调用的结果
     */
    Object invoke(ProviderHostAndPort providerHostAndPort, RpcContext rpcContext);
}
