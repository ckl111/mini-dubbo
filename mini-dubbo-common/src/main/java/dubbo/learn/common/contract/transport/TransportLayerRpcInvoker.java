package dubbo.learn.common.contract.transport;

import dubbo.learn.common.ProviderHostAndPort;
import dubbo.learn.common.RpcContext;

/**
 *
 * 本层为传输层，上层为exchange层，下层为序列化层。
 * 上层exchange，目前有一个默认实现，主要是完成同步转异步的操作。
 * 上层将具体的传输工作交给底层的传输层，比如netty和mina，然后在一个future上等待传输层完成工作
 *
 * 本层会完成实际的发送工作和接收返回响应的工作
 */
public interface TransportLayerRpcInvoker {

    /**
     *
     * @param providerHostAndPort 要调用的服务提供者的地址
     * @param rpcContext   rpc上下文，包含了要调用的参数等
     * @return  rpc调用的结果
     */
    Object invoke(ProviderHostAndPort providerHostAndPort, RpcContext rpcContext);
}
