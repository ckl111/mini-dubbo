package dubbo.learn.common;

import lombok.Data;

import java.lang.reflect.Method;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

@Data
public class RpcContext {
    private Object proxy;

    private Method method;

    private Object[] args;

    /**
     * 要调用的服务的全路径类名
     */
    private String serviceName;

    private String requestId;

    /**
     * 同步转异步时，从exchange层往下面的transport传输该引用
     */
    ConcurrentHashMap<String, CompletableFuture<Object>> requestId2futureMap;
}
