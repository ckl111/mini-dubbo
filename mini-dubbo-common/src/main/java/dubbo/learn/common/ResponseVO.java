package dubbo.learn.common;

import lombok.Data;

import java.util.List;

@Data
public class ResponseVO {
    private String requestId;

    private String content;

}
