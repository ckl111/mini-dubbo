package dubbo.learn.registry;

import dubbo.learn.common.ProviderHostAndPort;

import java.util.List;

public interface RemoteServiceRegistry {
    List<ProviderHostAndPort> getServiceProviderList(String serviceName);
}
