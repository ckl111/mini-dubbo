package dubbo.learn.client;

import dubbo.learn.CarCurrentLocationVO;
import dubbo.learn.IGpsLocationUpdateService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("consumer.xml");

        IGpsLocationUpdateService gpsLocationUpdateService = ctx.getBean(IGpsLocationUpdateService.class);
        CarCurrentLocationVO carCurrentLocationVO = new CarCurrentLocationVO();
        carCurrentLocationVO.setCarPlate("川A88888");
        carCurrentLocationVO.setDirection(2);
        carCurrentLocationVO.setSpeed(45);
        gpsLocationUpdateService.refreshLocation(carCurrentLocationVO);
    }

}
