package dubbo.learn;


public interface IGpsLocationUpdateService {
    /**
     * 车辆上报实时位置
     * @param currentLocationVO
     * @return
     */
    String refreshLocation(CarCurrentLocationVO currentLocationVO);
}
