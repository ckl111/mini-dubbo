package dubbo.learn.transport.netty4;

import dubbo.learn.common.ProviderHostAndPort;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class Netty4Client {
    private ProviderHostAndPort providerHostAndPort;

    private Object lockObj = new Object();

    private Channel channel;

    public Netty4Client(ProviderHostAndPort providerHostAndPort) {
        this.providerHostAndPort = providerHostAndPort;
        initChannel();
    }

    public void sendMessage(String messageContent) {
        synchronized (lockObj) {
            if (channel == null) {
                initChannel();
            }
        }
        ChannelFuture channelFuture = channel.writeAndFlush(messageContent);
        channelFuture.addListener(new GenericFutureListener<Future<? super Void>>() {
            @Override
            public void operationComplete(Future<? super Void> future) throws Exception {
                System.out.println("发送请求消息成功");
            }
        });
    }


    public void initChannel() {
        Bootstrap b = configBootStrap();
        ChannelFuture future = null;
        try {
            future = b.connect(providerHostAndPort.getHost(), providerHostAndPort.getPort()).sync();
            if (future.isSuccess()) {
                channel = future.channel();
                return;
            }
        } catch (InterruptedException e) {
            int counter = 1;
            while (true) {
                if (counter > 10) {
                    throw new RuntimeException("重连次数超限");
                }

                ChannelFuture channelFuture = b.connect(providerHostAndPort.getHost(), providerHostAndPort.getPort());
                if (channelFuture.isSuccess()) {
                    channel = channelFuture.channel();
                    return;
                }
                log.info("retrying...,the {} time" , counter++);
            }
        }

        throw new RuntimeException();
    }

    private Bootstrap configBootStrap() {
        EventLoopGroup group = new NioEventLoopGroup();
        Bootstrap b = new Bootstrap();
        b.group(group)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.TCP_NODELAY, true)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    public void initChannel(SocketChannel ch) throws Exception {
                        ChannelPipeline p = ch.pipeline();
                        p.addLast("lengthFieldPrepender", new LengthFieldPrepender(2));
                        p.addLast("lengthFieldBasedFrameDecoder",
                                new LengthFieldBasedFrameDecoder(
                                        65536, 0,
                                        2, 0, 2));
                        p.addLast("decoder", new StringDecoder());
                        p.addLast("encoder", new StringEncoder());
                        p.addLast(new ClientHandler());

                    }//拦截器设置
                });
        return b;
    }
}
